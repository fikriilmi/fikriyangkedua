from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import reverse
from django.shortcuts import redirect
from .forms import ScheduleForm
from .models import Event


# Create your views here.
def regis(request):
    return render(request, 'regis.html')


def index(request):
    return render(request, 'index.html')


def contact(request):
    return render(request, 'contact.html')

def personal_schedule(request):
	events = Event.objects.all().values()
	form = ScheduleForm()

	if request.method == "POST":
		form = ScheduleForm(request.POST)
		if form.is_valid():
			form.save()

		else :
			form = ScheduleForm()

	response = {'form': form, 'events': events}

	return render(request, 'form.html', response)

def delete_all_events (request):
	events = Event.objects.all().delete()
	form = ScheduleForm
	response = {'form':form, 'events': events}
	return render (request, 'form.html', response)


