from django import forms
from .models import Event
from django.forms import ModelForm

class ScheduleForm(forms.ModelForm):

	nama_kegiatan = forms.CharField(max_length=100)
	tempat = forms.CharField(max_length=200)
	kategori = forms.CharField(max_length=20)
	tanggal = forms.DateField()
	jam = forms.TimeField()

	class Meta:
		model = Event
		fields = ("nama_kegiatan", "tempat", "kategori", "tanggal", "jam",)
