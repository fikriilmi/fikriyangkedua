from django.db import models
from datetime import datetime, date

# Create your models here.
class Event(models.Model):
    nama_kegiatan = models.CharField(max_length=100)
    tempat = models.CharField(max_length=200)
    kategori = models.CharField(max_length=20)
    tanggal = models.DateField()
    jam = models.TimeField()


